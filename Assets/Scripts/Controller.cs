﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private GameObject square;
    private GameObject[] snake;
    [SerializeField] private int snakesize = 100;

    [SerializeField] private GameObject tile;
    private List<GameObject> tiles;
    private int tilecount = 0;
    private float probability = 0.2f;

    [SerializeField] private GameObject characterPrefab;
    private GameObject character;

    private int startplacenumcells = 8;
    private float side = Mathf.Sqrt(0.5f);
    private float deltaspeed = 0.3f;

    UserControl usercontrol;
    Points points;
    TextMesh statustext;

    // Start is called before the first frame update
    void Start()
    {        
        snake = new GameObject[snakesize + startplacenumcells];
        tiles = new List<GameObject>();
        generateSnake();
        generateBall();
        usercontrol = character.GetComponent<UserControl>();
        points = character.GetComponent<Points>();
        statustext = character.transform.GetChild(2).gameObject.GetComponent<TextMesh>();
    }

    private void generateSnake() {
        float x = 0;        
        float y = -4;

        int[] startplacex = new int[] { -1, 2, -2, 1, 0, 0, 1, -1 };
        int[] startplacey = new int[] { -1, 0, 0, 1, 2, -2, -1, 1 };

        for (int i = 0; i < startplacenumcells; i++)
        {
            snake[i] = Instantiate(square,
                new Vector3(x + side * startplacex[i], y + side * startplacey[i], 0),
                    Quaternion.Euler(0, 0, 45));
        }

        int rotoroutofstartplace = 0;

        for (int i = startplacenumcells; i < snakesize + startplacenumcells; i++) {
            int rotor = Random.Range(0, 2);
            if (rotor == 0)
                rotor = -1;
            if (i == startplacenumcells + 1)
                rotoroutofstartplace = rotor;
            if (i == startplacenumcells + 2)
                rotor = rotoroutofstartplace;

            snake[i] = Instantiate(square, new Vector3(x, y, 0), Quaternion.Euler(0, 0, 45));

            float puttile = Random.Range(0f, 1f);
            if (i > startplacenumcells + 3 && puttile <= probability)
            {
                GameObject t = Instantiate(tile, new Vector3(x, y, -1),
                    Quaternion.Euler(0, 0, rotor * 45));
                Renderer tilerenderer = t.GetComponent<Renderer>();
                tilerenderer.material.SetColor("_Color", Color.red);
                tiles.Add(t);
            }
            x += rotor * side;
            y += side;
        }
    }

    private void generateBall() {
        character = Instantiate(characterPrefab, new Vector3(0, -4 - 2 * side, -1),
            Quaternion.Euler(0, 0, 0));
        Renderer characterrenderer = character.GetComponent<Renderer>();
        characterrenderer.material.SetColor("_Color", Color.yellow);
    }

    private void destroySnake()
    {
        for (int i = 0; i < snakesize + startplacenumcells; i++)
        {
            Destroy(snake[i]);
        }
        for (int i = 0; i < tiles.Count; i++)
        {
            Destroy(tiles[i]);
        }
        tiles.Clear();
    }

    private void Update()
    {
        if (!characterOnSnakeBon()) {
            usercontrol.ismoving = false;
            usercontrol.moveside = "start";
            points.setText();
            character.transform.position = new Vector3(0, -4-2*side, -1);
            destroySnake();
            generateSnake();
        }
    }

    private float triangleSquare(float[] x, float[] y) {
        return 0.5f*Mathf.Abs((x[0] - x[2]) * (y[1] - y[2]) - (x[1] - x[2]) * (y[0] - y[2]));
    }

    private bool characterOnSnakeBon()
    {
        float x1 = character.transform.position.x;
        float y1 = character.transform.position.y;
        float epsilon = 0.03f;
        float squareArea = 1f;

        for (int i = 0; i < snakesize + startplacenumcells; i++)
        {
            float x = snake[i].transform.position.x;
            float y = snake[i].transform.position.y;

            float s1 = triangleSquare(new float[3] { x + side, x, x1 }, new float[3] { y, y + side, y1 });
            float s2 = triangleSquare(new float[3] { x + side, x, x1 }, new float[3] { y, y - side, y1 });
            float s3 = triangleSquare(new float[3] { x - side, x, x1 }, new float[3] { y, y + side, y1 });
            float s4 = triangleSquare(new float[3] { x - side, x, x1 }, new float[3] { y, y - side, y1 });

            if (Mathf.Abs(s1 + s2 + s3 + s4 - squareArea) < epsilon) {
                if (i == snakesize + startplacenumcells-1) {
                    usercontrol.speed += deltaspeed;
                    points.level++;
                    return false;
                }
                return true;
            }
        }

        statustext.text = "Game Over";
        return false;
    }
}
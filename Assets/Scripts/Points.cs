﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour
{
    private int points;
    TextMesh pointsonscreen;
    public int level { get; set; }

    private void Start()
    {
        pointsonscreen = gameObject.transform.GetChild(1).GetComponent<TextMesh>();
        level = 1;
    }

    void OnTriggerEnter(Collider other)
    {
        points++;
        setText();
    }

    public void setText() {
        pointsonscreen.text = "Level: " + level + ". Points: " + points.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserControl : MonoBehaviour
{
    private GameObject sphere;
    public float speed { get; set; }
    public bool ismoving { get; set; }
    public string moveside { get; set; }
    private Vector3 finalPosition;
    TextMesh statustext;

    void Start() {
        speed = 2f;
        moveside = "start";
        statustext = gameObject.transform.GetChild(2).gameObject.GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (moveside == "start") {
                statustext.text = "Go!";
                int side = Random.Range(0, 2);
                if (side == 0)
                    moveside = "left";
                else
                    moveside = "right";
            }
            Vector3 mousepoint = Input.mousePosition;
            mousepoint = Camera.main.ScreenToWorldPoint(mousepoint);
            ismoving = true;
            if (moveside == "left")
            {
                moveside = "right";
                finalPosition = new Vector3(transform.position.x + 100, transform.position.y + 100, -1);
            } else {
                moveside = "left";
                finalPosition = new Vector3(transform.position.x - 100, transform.position.y + 100, -1);
            }
        }

        if (ismoving)
            transform.position = Vector3.MoveTowards(transform.position, finalPosition, Time.deltaTime * speed);
    }
}